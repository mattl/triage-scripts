require 'gitlab'
require 'active_support/time'

module Waiting
  SLEEP_INTERVAL = 1

  def wait_a_while
    sleep(SLEEP_INTERVAL)
  end
end

module Gitlab
  module Issues
    class Gatherer
      include ::Waiting

      attr_reader :all_issues, :project_id

      def initialize(project_id)
        @project_id = project_id
        @all_issues = []
      end

      def gather_issues(options = {per_page: 200})
        unless @current_issues
          # Get first page
          @all_issues = []
          print_page_number(@all_issues)
          # assign to local as the call may fall over and we want to continue where we left off!
          new_issues = Gitlab.issues(@project_id, options)
          @current_issues = new_issues
          @all_issues << @current_issues
        else
          puts "Starting where we left off"
        end

        begin
          until !@current_issues.has_next_page? do
            print_page_number(@all_issues)
            # assign to local as the call may fall over and we want to continue where we left off!
            begin
              new_issues = @current_issues.next_page
              @current_issues = new_issues
              @all_issues << @current_issues
              print_stats(@all_issues)
            end
          end
        rescue Exception => msg
            wait_a_while
        end
        @all_issues.flatten!
        @current_issues = nil
        true
      end

      def export_issues
        @all_issues.flatten!
        csv_data = csv_start_data

        all_issues.each do |issue_object|
          csv_data << format_issue_data(issue_object)
        end

        CSV.open(csv_file_path, "wb") do |csv|
          csv_data.each { |row| csv << row }
        end; nil
      end

      def open_issues
        @open_issues ||= @all_issues.select { |issue| issue.state != 'closed' }
      end

      def closed_issues
        @closed_issues ||= @all_issues.select { |issue| issue.state == 'closed' }
      end

      private

      def format_issue_data(raw)
        issue = raw.instance_variable_get(:@hash)
        [
          issue["id"].to_s,
          issue["iid"].to_s,
          issue["title"],
          issue["state"],
          DateTime.parse(issue["created_at"]).to_date.to_s,
          DateTime.parse(issue["updated_at"]).to_date.to_s,
          issue["labels"].join(" "),
          issue["milestone"] ? issue["milestone"]["title"] : nil,
          issue["assignee"] ? issue["assignee"]["name"] : nil,
          issue["assignee"] ? issue["assignee"]["username"] : nil,
          issue["author"] ? issue["author"]["name"] : nil,
          issue["author"] ? issue["author"]["username"] : nil,
          issue["user_notes_count"],
          issue["upvotes"],
          issue["downvotes"]
        ]
      end

      def csv_start_data
        [csv_headers]
      end

      def csv_headers
        [
          "id","iid","title","state","created_at","updated_at",
          "labels","milestone","assignee_name","assignee_username",
          "author_name","author_username","user_notes_count",
          "upvotes","downvotes"
        ]
      end

      def csv_file_path
        File.join('.',"#{Date.today.to_s}-project-#{@project_id}-issues.csv")
      end

      def print_page_number(collection, collection_type = :issues)
        puts "Getting #{collection_type.to_s} page number #{collection.count + 1}"
      end

      def print_stats(collection, collection_type = :issues)
        puts "Currently gathered #{collection.flatten.count} #{collection_type.to_s}"
        puts "Unique #{collection_type.to_s}: #{collection.flatten.map(&:id).uniq.count}"
      end
    end
  end
end

Gitlab.configure do |config|
  config.endpoint       = 'https://gitlab.com/api/v3'
  config.private_token  = 'private_token'
end

GITLAB_CE_PROJECT_ID = 13083

glic = Gitlab::Issues::Gatherer.new(GITLAB_CE_PROJECT_ID)
glic.gather_issues
glic.export_issues
